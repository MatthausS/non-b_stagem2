# Non-b dna ploting scripts

This repository contains scripts for analyse  non-b dna bed files in order to plot violin and density curves of theses structures for differents communities

## Scripts

### 1. Intersection Script (Bash)

- **Script Name:** `intersect.sh`
- **Description:** This Bash script needs two BED files as arguments, then performs a bedtools intersect operation to put it in the 3rd argument Path.

### 2. Plotting Density plots (Python)

- **Script Name:** `density.py`
- **Description:** This python script is made to generate density plot from a community file and a intersect file.
- **Script Name:** `summary_table.py`
- **Description:** This python script is made to generate a table sumarizing the density plots from all the community file and all the intersect file.

### 3. Plotting Heatmap UPAC composition (Python)

- **Script Name:** `nonbheat_BIG.py`
- **Description:** This python script is made to generate density plot from a community file and a intersect file.

### 4. Plotting Barplot of TF (Python)
- **Script Name:** `TF_barplot_full.py`
- **Description:** This python script is made to generate TF ratio of presence from an intersect file.

## How to Use

the first script takes 3 arguments and need a bedtools environement:

**intersect.sh**  annot.bed file.bed ./paths_to_output

the second script generate density plot from an intersect.bed and an community.tsv

the Summary script generate summary heatmap from all intersect.bed and community.tsv

the third script generate heatmap of UPAC composition from an intersect.bed and an community.tsv

the forth script generate Barplot of Tf and structures co localisation from an intersect.bed



Dependencies

    bedtools
    pandas
    seaborn
    matplotlib
    plotly

