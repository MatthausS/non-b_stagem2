from collections import defaultdict

import pandas as pd

import sys

import numpy as np

import re

import random

def read_community_genes(filename):
    dgene = defaultdict(list)

    with open(filename, 'r') as file:
        nbli = 0
        for line in file:
            if nbli >= 1:
                linesplit = line.split("\t")
                gene_ids = linesplit[2].split(",")
                for gene_id in gene_ids:
                    dgene[linesplit[0]].append(gene_id.strip())
            nbli += 1

    return dgene


def df_maker(file_path):
    # Read the file into a DataFrame
    df = pd.read_csv(file_path, sep='\t', header=None)

    if df.shape[1] == 11:
        df.columns = ['chr_start', 'start', 'end', 'gene_id', 'gene_name', 'strand',
                      'chr_end', 'structure_start', 'structure_end', 'struct', 'sequ']
    elif df.shape[1] == 12:
        df.columns = ['chr_start', 'start', 'end', 'gene_id', 'gene_name', 'strand',
                      'chr_end', 'structure_start', 'structure_end', 'struct', 'score', 'sequ']
    elif df.shape[1] == 13:
        df.columns = ['chr_start', 'start', 'end', 'gene_id', 'gene_name', 'strand','chr_end', 'structure_start',
                       'structure_end', 'struct', 'score', 'structure_strand', 'sequ']

    
    # Calculate gene length and structure length
    df['gene_length'] = df['end'] - df['start'] + 1
    df['structure_length'] = df['structure_end'] - df['structure_start'] + 1

    # Convert 'sequ' to lowercase
    df['sequ_lower'] = df['sequ'].str.lower()

    # Define nucleotides and their combinations
    nucleotides = ['A', 'C', 'G', 'T', 'R', 'Y', 'S', 'W', 'K', 'M', 'B', 'D', 'H', 'V']

    # Count nucleotides
    for nucleotide in nucleotides:
        if nucleotide in ['R', 'Y', 'S', 'W', 'K', 'M', 'B', 'D', 'H', 'V']:  # For composite nucleotides, already calculated
            continue
        df[f'{nucleotide}_count'] = df['sequ_lower'].str.count(nucleotide.lower())
    print(df)
    # Calculate composite nucleotide counts based on simple nucleotide counts
    df['R_count'] = df['A_count'] + df['G_count']
    df['Y_count'] = df['C_count'] + df['T_count']
    df['S_count'] = df['C_count'] + df['G_count']
    df['W_count'] = df['A_count'] + df['T_count']
    df['K_count'] = df['T_count'] + df['G_count']
    df['M_count'] = df['A_count'] + df['C_count']
    df['B_count'] = df['C_count'] + df['G_count'] + df['T_count']
    df['D_count'] = df['A_count'] + df['G_count'] + df['T_count']
    df['H_count'] = df['A_count'] + df['C_count'] + df['T_count']
    df['V_count'] = df['A_count'] + df['G_count'] + df['C_count']
    print(df)
    # Calculate total nucleotides
    df['total_nucleotides'] = df['A_count'] + df['C_count'] + df['G_count'] + df['T_count']

    # Calculate percentage of each nucleotide
    for nucleotide in nucleotides:
        df[f'{nucleotide}_percent'] = (df[f'{nucleotide}_count'] / df['total_nucleotides']) * 100


    # Adjust the dictionary for aggregation to include the count operation correctly
    aggregated_functions = {'start': 'size'}  # Count occurrences using a column that exists for sure, like 'start'
    # Include mean percentage calculations for all nucleotides
    aggregated_functions.update({f'{n}_percent': 'mean' for n in nucleotides})
    # Include the sum calculations for gene_length and structure_length
    aggregated_functions.update({'gene_length': 'sum', 'structure_length': 'sum'})

    # Perform the grouping and aggregation
    grouped_df = df.groupby('gene_id', as_index=False).agg(aggregated_functions)

    # Rename the 'start' column to 'count' after aggregation to reflect the number of occurrences
    grouped_df = grouped_df.rename(columns={'start': 'count'})

    # Continue with the ratio and normalization calculations
    grouped_df['ratio%_struct'] = (grouped_df['structure_length'] / grouped_df['gene_length']) * 100
    min_ratio = grouped_df['ratio%_struct'].min()
    max_ratio = grouped_df['ratio%_struct'].max()
    grouped_df['norm'] = 10 * ((grouped_df['ratio%_struct'] - min_ratio) / (max_ratio - min_ratio))

    # Drop unnecessary columns if needed
    final_df = grouped_df.drop(columns=['gene_length', 'structure_length'])
    return final_df



def comm_filter(str_df, comu_dict):
    df_struct = df_maker(str_df)
    dgene = read_community_genes(comu_dict)
    
    dfs = []
    for k, v in dgene.items():
        community = k
        kc = df_struct[df_struct['gene_id'].isin(v)].copy()  # Make a copy to avoid modifying the original DataFrame
        kc['community'] = community  # Assigning the community value to the 'community' column
        dfs.append(kc)  # Append the modified DataFrame to the list
    
    result_df = pd.concat(dfs, ignore_index=True)
    
    # Calculate the threshold as the count value at the 90th percentile
    threshold = result_df['count'].quantile(0.9)
    
    # Drop rows with count values higher than the threshold
    result_df = result_df[result_df['count'] <= threshold]
    return result_df

def comm_filter_norm(str_df, comu_dict):
    df_struct=df_maker(str_df)
    dgene=read_community_genes(comu_dict)
    
    dfs = []
    for k, v in dgene.items():
        community = k
        kc = df_struct[df_struct['gene_id'].isin(v)].copy()  # Make a copy to avoid modifying the original DataFrame
        kc['community'] = community  # Assigning the community value to the 'community' column
        dfs.append(kc)  # Append the modified DataFrame to the list

    result_df = pd.concat(dfs, ignore_index=True)
    return result_df


import os



def nucleotide_sum_by_community(str_file, commu_file):
    result_df = comm_filter(str_file, commu_file)
    # Group by 'community' and calculate mean nucleotide percentages
    nucleotides = ['A_percent', 'C_percent', 'G_percent', 'T_percent', 'R_percent', 'Y_percent', 'S_percent', 'W_percent', 'K_percent', 'M_percent', 'B_percent', 'D_percent', 'H_percent', 'V_percent']
    nucleotide_sums = result_df.groupby('community')[nucleotides].mean().reset_index()
    counts_sum = result_df.groupby('community')['count'].sum().reset_index(name='count')
    nucleotide_sums = pd.merge(nucleotide_sums, counts_sum, on='community')

    # Calculate global mean for each nucleotide
    global_means = result_df[nucleotides].mean()
    print(global_means)
    print(result_df)
    # Calculate relative means and add as new columns
    for nucleotide in nucleotides:
        relative_mean_col = f'{nucleotide}_rel'
        nucleotide_sums[relative_mean_col] = nucleotide_sums[nucleotide] - global_means[nucleotide]

    # Further processing...
    num_unique_communities = len(nucleotide_sums['community'])

    if num_unique_communities == 10:
        # Define the desired community order
        community_order = ['Lamina','Near-Lm2', 'Near-Lm1','Interior-Act3', 'Lamina-Like','Interior-Repr1', 'Interior-Act2', 
                        'Interior-Repr2', 'Interior-Act1', 'Speckle']
    elif num_unique_communities == 23:
        # Define the desired community order
        community_order = ['G1','G2','G3','G4','G5','G6','G7','G8','G9','G10','G11','G12','G13','G14','G15','G16','G17','G18','G19','G20','G21','G22','G23']

    if num_unique_communities in [10, 23]:
        # Set the 'community' column to a categorical type with the specified order
        nucleotide_sums['community'] = pd.Categorical(nucleotide_sums['community'], categories=community_order, ordered=True)
        # Sort the DataFrame by the 'community' column according to the defined order
        nucleotide_sums = nucleotide_sums.sort_values('community')

    # Modify the community names by appending the count values
    nucleotide_sums['community'] = nucleotide_sums['community'].astype(str) + "_" + nucleotide_sums['count'].astype(str)

    # Create two dataframes, one for percentages and another for relative means
    percent_columns = ['community'] + nucleotides
    rel_columns = ['community'] + [f'{nucleotide}_rel' for nucleotide in nucleotides]

    percent_df = nucleotide_sums[percent_columns]
    rel_df = nucleotide_sums[rel_columns]

    # Remove the 'count' column as it's no longer needed
    nucleotide_sums.drop('count', axis=1, inplace=True)
    percent_df = percent_df.set_index('community')
    rel_df = rel_df.set_index('community')
    return rel_df



def generate_gene_communities(annotation_bedfile, outputfile_path):
    # Read the BED file and extract the 4th column containing gene IDs
    df = pd.read_csv(annotation_bedfile, sep='\t', header=None, usecols=[3])
    unique_genes = list(df[3].unique())

    communities_data = []  # To collect community data

    for community_count in range(1, 16):  # Limit to 15 communities
        if len(unique_genes) < 600:
            break  # Break if fewer than 600 genes are left
        community_size = random.randint(600, min(1200, len(unique_genes)))
        selected_genes = random.sample(unique_genes, community_size)

        # Remove selected genes from the unique_genes list
        unique_genes = [gene for gene in unique_genes if gene not in selected_genes]

        communities_data.append({
            'community': f'RdM{community_count}',
            'nodes': len(selected_genes),
            'genes': ','.join(selected_genes)
        })

    communities_df = pd.DataFrame(communities_data)
    communities_df.to_csv(outputfile_path, sep='\t', index=False)



import seaborn as sns 
import matplotlib.pyplot as plt
from matplotlib.patches import Patch
from matplotlib.backends.backend_pdf import PdfPages


structures = {
    "G4": ["non-b_db_GQ", "allquads", "G4_HG38", "PQSM2a_all_chromosomes"],
    "Zdna":["non-b_db_Z","DZDNA_HG38","gquadR_ZDNA"],
    "HDNA": ["non-b_db_Triplex", "gquadR_HDNA"],
    "Slipped":["non-b_db_slipped","gquadR_slipped"],
    "TFO":["gquadR_TFO"],
    "DNA:DNA:RNA":["triplex"],
    "i-motif":["PiMS2a_all_chromosomes","IMSEEKER"],
    "Cruciforme":["non-b_db_cruci","palindromes_cruci"]
}

Repeats = {
    "Satellites":["dfam_Satellite","Repeatmasker_Satellite"],
    "APR":["non-b_db_APR","gquadR_APR"],
    "STR":["non-b_db_STR","gquadR_STR"],
    "DR":["non-b_db_DR"],
    "IR":["non-b_db_IR","palindromes"],
    "MR": ["non-b_db_MR"]
}




import os
#from .heatmap_gene_content import heatmap_fig

#nucleotide_sum_by_community("/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_palindromes.bed", "/home/matthaus/Documents/data/community_SPIN.txt")


# Community files mapping
communities = {
    "/home/matthaus/Documents/data/community_SPIN.txt": "spin",
    "/home/matthaus/Documents/data/community/dendro_gene_nt_ward_euclidean_tr50.0_Sordered.txt": "community23",
    "/home/matthaus/Documents/data/community/Rdcommu.txt": "rand",
}

links = [
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/structures/intersect_allquads.bed",
    ]
IR="/home/matthaus/Documents/data/basedo/BED_files/intersect/reshape/intersect_gene_non-bdb_DR.bed"
comu="/home/matthaus/Documents/data/community_SPIN.txt"

df1=nucleotide_sum_by_community(IR,comu)
print(df1)
df1=df1.drop(columns=['R_percent_rel','Y_percent_rel','S_percent_rel','V_percent_rel','B_percent_rel','M_percent_rel','Y_percent_rel','K_percent_rel','W_percent_rel','D_percent_rel','H_percent_rel'])

rdblu="Picnic"
from .heatmap_gene_content import heatmap_fig

heatmap_fig(df1, "./results/nonb/summary/heatmap_comp_DR", rdblu, False, True, 2, None)

'''


# Loop through each community and link combination
for community_path, community_id in communities.items():
    for link in links:
        # Extract a meaningful identifier from the link file name
        link_id = os.path.basename(link).split('sect_')[1]  # Adjust index as needed based on your file naming convention
        
        # Create a base file name for the output
        base_file_name = f"{link_id}_{community_id}"
        
        # Generate heatmaps
        # Note: You will need to replace the following lines with your actual data processing and heatmap generation code.
        df, dfrel = nucleotide_sum_by_community(link, community_path)
        print(df)
        df.drop(['R_percent', 'Y_percent', 'S_percent', 'W_percent','K_percent', 'M_percent', 'B_percent','D_percent', 'H_percent', 'V_percent'], axis=1, inplace=True)
        heatmap_fig(df, f"./results/nonb/{base_file_name}", "Picnic", False, False, 2, None)
        print(f"Generated heatmaps for {base_file_name}")  # Placeholder print statement
        
links = [
    "/home/matthaus/Documents/data/basedo/repeat/Human.RepeatMasker.map/parsedBED/intersect_Repeatmasker_Retroposon.bed",
    "/home/matthaus/Documents/data/basedo/repeat/Human.RepeatMasker.map/parsedBED/intersect_Repeatmasker_LTR.bed",
    "/home/matthaus/Documents/data/basedo/repeat/Human.RepeatMasker.map/parsedBED/intersect_Repeatmasker_DNA.bed",
    "/home/matthaus/Documents/data/basedo/repeat/Human.RepeatMasker.map/parsedBED/intersect_Repeatmasker_Low_complexity.bed",
    "/home/matthaus/Documents/data/basedo/repeat/Human.RepeatMasker.map/parsedBED/intersect_Repeatmasker_Satellite.bed",
    "/home/matthaus/Documents/data/basedo/repeat/Human.RepeatMasker.map/parsedBED/intersect_Repeatmasker_Simple_repeat.bed",
    "/home/matthaus/Documents/data/basedo/repeat/dfam/parsedBED/intersect_dfam_DNA.bed",
    "/home/matthaus/Documents/data/basedo/repeat/dfam/parsedBED/intersect_dfam_LTR.bed",
    "/home/matthaus/Documents/data/basedo/repeat/dfam/parsedBED/intersect_dfam_Retrotroposon.bed",
    "/home/matthaus/Documents/data/basedo/repeat/dfam/parsedBED/intersect_dfam_Satellite.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_EG4_HG38.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_G4_HG38.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_gquadR_APR.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_gquadR_HDNA.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_gquadR_slipped.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_gquadR_STR.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_gquadR_TFO.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_gquadR_ZDNA.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_IMSEEKER.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_non-b_db_APR.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_non-b_db_DR.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_non-b_db_GQ.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_non-b_db_IR.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_non-b_db_MR.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_non-b_db_STR.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_non-b_db_Z.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_palindromes.bed"
]
#generate_gene_communities('/home/matthaus/Documents/data/annot/genes.bed', '/home/matthaus/Documents/data/community/Rdcommu.txt')


spin="/home/matthaus/Documents/data/community_SPIN.txt"
community23="/home/matthaus/Documents/data/community/dendro_gene_nt_ward_euclidean_tr50.0_Sordered.txt"



rdblu="Picnic"
from .heatmap_gene_content import heatmap_fig

rand='/home/matthaus/Documents/data/community/Rdcommu.txt'

link="/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_PiMS2a_all_chromosomes.bed"
df = nucleotide_sum_by_community(link, community)
df = df.set_index('community')
heatmap_fig(df, "./results/nonb/heatmap_PiMS2a", rdblu, False, True, 2, None)


df = nucleotide_sum_by_community(link, community23)
df = df.set_index('community')
heatmap_fig(df, "./results/nonb/heatmap23_PiMS2a", rdblu, False, True, 2, None)

df = nucleotide_sum_by_community(link, rand)
df = df.set_index('community')
heatmap_fig(df, "./results/nonb/heatmapRand_PiMS2a", rdblu, False, True, 2, None)


#Assuming you have a list of links and the necessary functions and libraries already imported
links = [
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_EG4_HG38.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_gquadR_slipped.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_gquadR_STR.bed"
]


for link in links:
    # Use the link as the first argument in nucleotide_sum_by_community
    df = nucleotide_sum_by_community(link, community)
    dfrand = nucleotide_sum_by_community(link, rand)
    df23 = nucleotide_sum_by_community(link, community23) # Assuming 'community' is defined somewhere
    df = df.set_index('community')
    dfrand = dfrand.set_index('community')
    df23 = df23.set_index('community')

    # Extract the specific part of the link for the output filename
    match = re.search(r"intersect_(.*?)\.bed", link)
    name_part = match.group(1)  # This captures the string between "intersect_" and ".bed"

    # Generate the heatmap for df with a specific naming convention
    output_path_df = f"./results/nonb/heatmap_{name_part}"
    heatmap_fig(df, output_path_df, rdblu, False, True, 2, None)

    # Generate the heatmap for dfrand with a specific naming convention
    output_path_dfrand = f"./results/nonb/heatmapRand_{name_part}"
    heatmap_fig(dfrand, output_path_dfrand, rdblu, False, True, 2, None)

    # Generate the heatmap for df23 with a specific naming convention
    output_path_df23 = f"./results/nonb/heatmap23_{name_part}"
    heatmap_fig(df23, output_path_df23, rdblu, False, True, 2, None)



links = [
    "/home/matthaus/Documents/data/basedo/repeat/Human.RepeatMasker.map/parsedBED/intersect_Repeatmasker_Retroposon.bed",
    "/home/matthaus/Documents/data/basedo/repeat/Human.RepeatMasker.map/parsedBED/intersect_Repeatmasker_LTR.bed",
    "/home/matthaus/Documents/data/basedo/repeat/Human.RepeatMasker.map/parsedBED/intersect_Repeatmasker_DNA.bed",
    "/home/matthaus/Documents/data/basedo/repeat/Human.RepeatMasker.map/parsedBED/intersect_Repeatmasker_LINE.bed",
    "/home/matthaus/Documents/data/basedo/repeat/Human.RepeatMasker.map/parsedBED/intersect_Repeatmasker_Low_complexity.bed",
    "/home/matthaus/Documents/data/basedo/repeat/Human.RepeatMasker.map/parsedBED/intersect_Repeatmasker_Satellite.bed",
    "/home/matthaus/Documents/data/basedo/repeat/Human.RepeatMasker.map/parsedBED/intersect_Repeatmasker_Simple_repeat.bed",
    "/home/matthaus/Documents/data/basedo/repeat/Human.RepeatMasker.map/parsedBED/intersect_Repeatmasker_SINE.bed",
    "/home/matthaus/Documents/data/basedo/repeat/dfam/parsedBED/intersect_dfam_DNA.bed",
    "/home/matthaus/Documents/data/basedo/repeat/dfam/parsedBED/intersect_dfam_LTR.bed",
    "/home/matthaus/Documents/data/basedo/repeat/dfam/parsedBED/intersect_dfam_Retrotroposon.bed",
    "/home/matthaus/Documents/data/basedo/repeat/dfam/parsedBED/intersect_dfam_Satellite.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_EG4_HG38.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_G4_HG38.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_gquadR_APR.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_gquadR_HDNA.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_gquadR_slipped.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_gquadR_STR.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_gquadR_TFO.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_gquadR_ZDNA.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_IMSEEKER.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_non-b_db_APR.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_non-b_db_DR.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_non-b_db_GQ.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_non-b_db_IR.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_non-b_db_MR.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_non-b_db_STR.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_non-b_db_Z.bed",
    "/home/matthaus/Documents/data/basedo/BED_files/intersect/intersect_palindromes.bed"
]
'''