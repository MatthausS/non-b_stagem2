from collections import defaultdict

import pandas as pd

import matplotlib.pyplot as plt

import seaborn as sns

import numpy as np

import os

import re

from matplotlib.backends.backend_pdf import PdfPages
import plotly
import plotly.graph_objects as go

from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler

def df_maker_density(file_path):
    # Read the file into a DataFrame
    df = pd.read_csv(file_path, sep='\t', header=None)

    if df.shape[1] == 11:
        df.columns = ['chr_start', 'start', 'end', 'gene_id', 'gene_name', 'strand',
                      'chr_end', 'structure_start', 'structure_end', 'struct', 'sequ']
    #df['gene_id'] = df['gene_id'].str.split('_').str[0]# IF exon needed to be treated on their gene

    # Calculate gene length and structure length
    df['gene_length'] = df['end'] - df['start'] + 1
    df['structure_length'] = df['structure_end'] - df['structure_start'] + 1
    # Adjust the dictionary for aggregation to include the count operation correctly
    aggregated_functions = {
        'start': 'size',  # Count occurrences using a column that exists for sure, like 'start'
        'gene_length': 'first',  # Keep the first occurrence of gene_length for each gene_id
        'structure_length': 'sum'  # Sum the structure_length for each gene_id
        }
    # Perform the grouping and aggregation
    grouped_df = df.groupby('gene_id', as_index=False).agg(aggregated_functions)

    # Rename the 'start' column to 'count' after aggregation to reflect the number of occurrences
    grouped_df = grouped_df.rename(columns={'start': 'count'})
    grouped_df['count_norm'] = grouped_df['count'] / grouped_df['gene_length']
    grouped_df['length_norm'] = grouped_df['structure_length'] / grouped_df['gene_length']


    # Drop unnecessary columns if needed
    grouped_df=grouped_df.drop(columns=['gene_length', 'structure_length'])
    return grouped_df

IR="/home/matthaus/Documents/data/basedo/BED_files/intersect/reshape/intersect_gene_non-bdb_IR.bed"
comu="/home/matthaus/Documents/data/community_SPIN.txt"

#print(df_maker_density(IR))

def community_df(file):
    df = pd.read_csv(file, sep='\t')
    df=df.drop(columns=['nodes', 'project'])
    df = df.set_index('community')['genes'].str.split(', ', expand=True).stack().reset_index(level=1, drop=True).reset_index(name='genes')
    #df.to_csv("Documents/data/annot/df_SPIN.csv", sep='\t', index=False)
    print(df)

#df_commu = pd.read_csv("Documents/data/annot/df_SPIN.csv", sep='\t')
#merged_df = pd.merge(df_commu, df_IR, left_on='genes', right_on='gene_id', how='inner')
#print(merged_df)


intersect="/home/matthaus/Documents/data/basedo/BED_files/intersect/reshape/struct"
files = [file for file in os.listdir(intersect) if "_gene_" in file and os.path.isfile(os.path.join(intersect, file))]
df_commu = pd.read_csv("/home/matthaus/Documents/data/annot/df_SPIN.csv", sep='\t')

def big_df_ct(files):

    a=0

    for fichier in files:
        suffix=fichier.split("gene_")[1].split(".bed")[0]
        fichier=f"{intersect}{fichier}"
        df_courant=df_maker_density(fichier)

        #df_merged = pd.merge(df_commu, df_courant, left_on='genes', right_on='gene_id', how='inner')

        df_courant=df_courant.drop(columns=['count_norm', 'length_norm'])

        df_courant[f"{suffix}"]=df_courant["count"]

        df_courant=df_courant.drop(columns=['count'])
        df_courant[f"{suffix}"] = df_courant[f"{suffix}"].astype(float)
        if a==0:
            df_final=df_courant
        if a>=1:
            df_final = pd.merge(df_final, df_courant[['gene_id', f"{suffix}"]], on='gene_id', how='outer')
            df_final.fillna(0, inplace=True)

        a+=1
        print(df_final)
    #df_final.to_csv("/home/matthaus/Documents/data/basedo/df/df_gene_count_nocomu.csv", sep='\t', index=False)
    df_final = pd.merge(df_commu, df_final, left_on='genes', right_on='gene_id', how='inner')
    df_final=df_final.drop(columns=['genes'])
    #df_final.to_csv("/home/matthaus/Documents/data/basedo/df/df_gene_count.csv", sep='\t', index=False)
    print(df_final)
    return df_final

def big_df_ct_norm(files):

    a=0

    for fichier in files:
        suffix=fichier.split("gene_")[1].split(".bed")[0]
        fichier=f"{intersect}{fichier}"
        df_courant=df_maker_density(fichier)

        #df_merged = pd.merge(df_commu, df_courant, left_on='genes', right_on='gene_id', how='inner')

        df_courant=df_courant.drop(columns=['count', 'length_norm'])

        df_courant[f"{suffix}"]=df_courant["count_norm"]

        df_courant=df_courant.drop(columns=['count_norm'])
        df_courant[f"{suffix}"] = df_courant[f"{suffix}"].astype(float)
        if a==0:
            df_final=df_courant
        if a>=1:
            df_final = pd.merge(df_final, df_courant[['gene_id', f"{suffix}"]], on='gene_id', how='outer')
            df_final.fillna(0, inplace=True)

        a+=1
        print(df_final)

    #df_final.to_csv("/home/matthaus/Documents/data/basedo/df/df_gene_count_norm_nocomu.csv", sep='\t', index=False)
    df_final = pd.merge(df_commu, df_final, left_on='genes', right_on='gene_id', how='inner')
    df_final=df_final.drop(columns=['genes'])
    #df_final.to_csv("/home/matthaus/Documents/data/basedo/df/df_gene_count_norm.csv", sep='\t', index=False)
    print(df_final)
    return df_final

def big_df_ct_norm_length(files):

    a=0

    for fichier in files:
        suffix=fichier.split("gene_")[1].split(".bed")[0]
        fichier=f"{intersect}{fichier}"
        df_courant=df_maker_density(fichier)

        #df_merged = pd.merge(df_commu, df_courant, left_on='genes', right_on='gene_id', how='inner')

        df_courant=df_courant.drop(columns=['count', 'count_norm'])

        df_courant[f"{suffix}"]=df_courant["length_norm"]

        df_courant=df_courant.drop(columns=['length_norm'])
        df_courant[f"{suffix}"] = df_courant[f"{suffix}"].astype(float)
        if a==0:
            df_final=df_courant
        if a>=1:
            df_final = pd.merge(df_final, df_courant[['gene_id', f"{suffix}"]], on='gene_id', how='outer')
            df_final.fillna(0, inplace=True)

        a+=1
        print(df_final)

    #df_final.to_csv("/home/matthaus/Documents/data/basedo/df/df_gene_count_norm_length_nocomu.csv", sep='\t', index=False)
    df_final = pd.merge(df_commu, df_final, left_on='genes', right_on='gene_id', how='inner')
    df_final=df_final.drop(columns=['genes'])
    #df_final.to_csv("/home/matthaus/Documents/data/basedo/df/df_gene_count_norm_length.csv", sep='\t', index=False)
    print(df_final)
    return df_final
rdblu="Picnic"
#df1=big_df_ct(files)


#df1=big_df_ct_norm_length(files)
df1=pd.read_csv("/home/matthaus/Documents/data/basedo/df/df_gene_count_norm.csv", sep='\t')

print(df1)
df1=df1.drop(columns=['allquads_G4','endoquad_G4','gquadR_HDNA','gquadR_slipped','gquadR_TFO','gquadR_ZDNA','imgrinder_GQ','imgrinder_IM','imseeker_imotif','non-bdb_cruci','non-bdb_GQ','non-bdb_slipped','non-bdb_triplex','non-bdb_Z','palindrome_cruci'])
#df1=df1.drop(columns=['gquadR_STR','non-bdb_APR','non-bdb_DR','non-bdb_IR','non-bdb_MR','non-bdb_STR','palindrome'])

df1=df1.drop(columns=['gene_id'])

df1=df1.set_index('community')
df1=df1.groupby('community').median()


print(df1)

rescale_df=(df1 - df1.min()) / (df1.max() - df1.min()) * 9 + 1
print(rescale_df)
order_community = [
    "Lamina", "Near-Lm2", "Near-Lm1", "Interior-Act3", "Lamina-Like",
    "Interior-Repr1", "Interior-Act2", "Interior-Repr2", "Interior-Act1", "Speckle"
]
rescale_df = rescale_df.reindex(order_community)
col_means = df1.mean()
df_normalized = df1 - col_means

df1=df1.reindex(order_community)

df_standardized = (df1 - df1.mean()) / df1.std()
df_standardized=df_standardized.reindex(order_community)

df_normalized=df_normalized.reindex(order_community)

from .heatmap_gene_content import heatmap_fig

heatmap_fig(df_standardized, "./results/nonb/summary/NR_heatmap_repeat", rdblu, False, True, 2, None)
