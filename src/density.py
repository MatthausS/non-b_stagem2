from collections import defaultdict

import pandas as pd

import matplotlib.pyplot as plt

import seaborn as sns

import numpy as np

import os

from matplotlib.backends.backend_pdf import PdfPages

import plotly.graph_objects as go

intersect="/home/matthaus/Documents/data/basedo/BED_files/intersect/reshape/"

files= [file for file in os.listdir(intersect) if os.path.isfile(os.path.join(intersect, file))]

comu="/home/matthaus/Documents/data/community_SPIN.txt"

def read_community_genes(filename):
    """read a community genes file to return a dict as dgene[community]={ENS0001,ENS00013,ENS0876}

    Args:
        filename (_type_): filename like comu="/home/matthaus/Documents/data/community_SPIN.txt"

    Returns:
        _type_: dgene[community]={ENS0001,ENS00013,ENS0876}
    """
    dgene = defaultdict(list)

    with open(filename, 'r') as file:
        nbli = 0
        for line in file:
            if nbli >= 1:
                linesplit = line.split("\t")
                gene_ids = linesplit[-2].split(",")
                for gene_id in gene_ids:
                    dgene[linesplit[0]].append(gene_id.strip())
            nbli += 1

    return dgene


def df_maker_percent(file_path):
    """
    GOUPED BY MEAN OF THE %
    """
    # Read the file into a DataFrame
    df = pd.read_csv(file_path, sep='\t', header=None)

    if df.shape[1] == 11:
        df.columns = ['chr_start', 'start', 'end', 'gene_id', 'gene_name', 'strand',
                      'chr_end', 'structure_start', 'structure_end', 'struct', 'sequ']

    # Convert 'sequ' to lowercase
    df['sequ_lower'] = df['sequ'].str.lower()

    # Define nucleotides and their combinations
    nucleotides = ['A', 'C', 'G', 'T', 'R', 'Y', 'S', 'W', 'K', 'M', 'B', 'D', 'H', 'V']

    # Count nucleotides
    for nucleotide in nucleotides:
        if nucleotide in ['R', 'Y', 'S', 'W', 'K', 'M', 'B', 'D', 'H', 'V']:  # For composite nucleotides, already calculated
            continue
        df[f'{nucleotide}_count'] = df['sequ_lower'].str.count(nucleotide.lower())

    # Calculate composite nucleotide counts based on simple nucleotide counts
    df['R_count'] = df['A_count'] + df['G_count']
    df['Y_count'] = df['C_count'] + df['T_count']
    df['S_count'] = df['C_count'] + df['G_count']
    df['W_count'] = df['A_count'] + df['T_count']
    df['K_count'] = df['T_count'] + df['G_count']
    df['M_count'] = df['A_count'] + df['C_count']
    df['B_count'] = df['C_count'] + df['G_count'] + df['T_count']
    df['D_count'] = df['A_count'] + df['G_count'] + df['T_count']
    df['H_count'] = df['A_count'] + df['C_count'] + df['T_count']
    df['V_count'] = df['A_count'] + df['G_count'] + df['C_count']
    # Calculate total nucleotides
    df['total_nucleotides'] = df['A_count'] + df['C_count'] + df['G_count'] + df['T_count']


    # Calculate percentage of each nucleotide
    for nucleotide in nucleotides:
        df[f'{nucleotide}_percent'] = (df[f'{nucleotide}_count'] / df['total_nucleotides']) * 100



    # Adjust the dictionary for aggregation to include the count operation correctly
    aggregated_functions = {'start': 'size'}  # Count occurrences using a column that exists for sure, like 'start'
    # Include mean percentage calculations for all nucleotides
    aggregated_functions.update({f'{n}_percent': 'mean' for n in nucleotides})
    # Include the sum calculations for gene_length and structure_length
    #aggregated_functions.update({'gene_length': 'sum', 'structure_length': 'sum'})

    # Perform the grouping and aggregation
    percent_df = df.groupby('gene_id', as_index=False).agg(aggregated_functions)
  

    # Rename the 'start' column to 'count' after aggregation to reflect the number of occurrences
    percent_df = percent_df.rename(columns={'start': 'count'})


    return percent_df
#print(df_maker_percent(IR))


def df_maker_density(file_path):
    # Read the file into a DataFrame
    df = pd.read_csv(file_path, sep='\t', header=None)

    if df.shape[1] == 11:
        df.columns = ['chr_start', 'start', 'end', 'gene_id', 'gene_name', 'strand',
                      'chr_end', 'structure_start', 'structure_end', 'struct', 'sequ']
    #df['gene_id'] = df['gene_id'].str.split('_').str[0]# IF exon needed to be treated on their gene

    # Calculate gene length and structure length
    df['gene_length'] = df['end'] - df['start'] + 1
    df['structure_length'] = df['structure_end'] - df['structure_start'] + 1
    # Adjust the dictionary for aggregation to include the count operation correctly
    aggregated_functions = {
        'start': 'size',  # Count occurrences using a column that exists for sure, like 'start'
        'gene_length': 'first',  # Keep the first occurrence of gene_length for each gene_id
        'structure_length': 'sum'  # Sum the structure_length for each gene_id
        }
    # Perform the grouping and aggregation
    grouped_df = df.groupby('gene_id', as_index=False).agg(aggregated_functions)

    # Rename the 'start' column to 'count' after aggregation to reflect the number of occurrences
    grouped_df = grouped_df.rename(columns={'start': 'count'})
    grouped_df['count_norm'] = grouped_df['count'] / grouped_df['gene_length']
    grouped_df['length_norm'] = grouped_df['structure_length'] / grouped_df['gene_length']


    # Drop unnecessary columns if needed
    grouped_df=grouped_df.drop(columns=['gene_length', 'structure_length'])
    return grouped_df

#print(df_maker_density(IRex))

def exon_com (comu_dict):
    file="/home/matthaus/Documents/data/annot/exons.bed"
    dgene = read_community_genes(comu_dict)
    df = pd.read_csv(file, sep='\t', header=None)
    df.columns = ['chr', 'start', 'end', 'exon_id', 'score', 'strand']
    # Step 1: Keep only the exon_id column
    df = df[['exon_id']]

    # Step 2: Create the gene_id column based on exon_id
    df['gene_id'] = df['exon_id'].apply(lambda x: x.split('_')[0])
    return df


    

def comm_filter(str_df, comu_dict):
    df_struct = df_maker_density(str_df)
    print(str_df.split("/")[-1])
    print("Size of Original DataFrame: (intersect) ", df_struct.shape)
    dgene = read_community_genes(comu_dict)
    dfs = []
    for k, v in dgene.items():
        community = k
        # Initialize a DataFrame for all genes in the community with default values
        all_genes_df = pd.DataFrame({'gene_id': v})
        # Merge with df_struct to find matches and fill missing values with 0
        kc = pd.merge(all_genes_df, df_struct, on='gene_id', how='left').fillna({'count': 0, 'structure_length': 0})
        kc['community'] = community  # Assigning the community value
        print(community)
        dfs.append(kc)  # Append the modified DataFrame to the list
    result_df = pd.concat(dfs, ignore_index=True)
    result_df = result_df.fillna(0)
    # Calculate the threshold as the count value at the 90th percentile
    print("Size of DataFrame: ", result_df.shape)
    print("Number of times 'count' is 0: ", (result_df['count'] == 0).sum())
    return result_df








    dfs = []
    for k, v in dgene.items():
        community = k
        all_genes_df = pd.DataFrame({'gene_id': v})
        # Note: The merge now uses 'actual_gene_id' for matching
        kc = pd.merge(merged_df,all_genes_df, left_on='actual_gene_id', right_on='gene_id', how='left')
        kc = kc.fillna({'count': 0, 'structure_length': 0})  # Assuming count and structure_length need filling
        kc['community'] = community
        # Ensure we drop the temporary 'actual_gene_id' column if not needed in the final output
        kc.drop('actual_gene_id', axis=1, inplace=True)
        #kc.drop('gene_id_x', axis=1, inplace=True)        
        dfs.append(kc)
    
    result_df = pd.concat(dfs, ignore_index=True)
    #print(result_df)
    result_df = result_df.fillna(0)    
    #print("Size of DataFrame: ", result_df.shape)
    #print("Number of times 'count' is 0: ", (result_df['count'] == 0).sum())
    return result_df


def plot_all(str_df, comu_dict, pdf):
    result_df = comm_filter(str_df, comu_dict)

    if gene == "intersect_gene_":
        threshold = result_df['count'].quantile(0.9)
        # Drop rows with count values higher than the threshold
        result_df = result_df[result_df['count'] <= threshold]
    
    file_basename = os.path.basename(str_df)
    structure_name = os.path.splitext(file_basename)[0]
    sns.set(style="whitegrid")
    communities = result_df['community'].unique()
    n_colors = len(communities)
    palette = sns.color_palette("coolwarm", n_colors=n_colors)
    
    fig, axs = plt.subplots(2, 3, figsize=(20, 12))

    sns.kdeplot(data=result_df, x="count", hue="community", fill=True, common_norm=False, palette=palette, ax=axs[0, 0])
    axs[0, 0].set_title(f"Density Curves of Counts for Each Community {community_name}- {source}")

    sns.kdeplot(data=result_df, x="count_norm", hue="community", fill=True, common_norm=False, palette=palette, ax=axs[0, 1])
    axs[0, 1].set_title(f"Density Curves of Counts Normalised {community_name}- {source}")

    sns.kdeplot(data=result_df, x="length_norm", hue="community", fill=True, common_norm=False, palette=palette, ax=axs[0, 2])
    axs[0, 2].set_title(f"Density Curves of Length Normalised {community_name}- {source}")

    sns.violinplot(data=result_df, x="community", y="count", inner="quartile", palette="Set3", ax=axs[1, 0])
    axs[1, 0].set_title(f"Violin Plot of Count for Each Community {community_name}- {source}")
    axs[1, 0].tick_params(axis='x', rotation=45)

    sns.violinplot(data=result_df, x="community", y="count_norm", inner="quartile", palette="Set3", ax=axs[1, 1])
    axs[1, 1].set_title(f"Violin Plot of Counts Normalised  {community_name}- {source}")
    axs[1, 1].tick_params(axis='x', rotation=45)

    sns.violinplot(data=result_df, x="community", y="length_norm", inner="quartile", palette="Set3", ax=axs[1, 2])
    axs[1, 2].set_title(f"Violin Plot of Length Normalised {community_name}- {source}")
    axs[1, 2].tick_params(axis='x', rotation=45)    
    plt.tight_layout()
    pdf.savefig(fig)  # Save the current figure into the PDF file
    #plt.show()
    plt.close()



communities = {
    "/home/matthaus/Documents/data/community_SPIN.txt": "spin",
    "/home/matthaus/Documents/data/community/dendro_gene_nt_ward_euclidean_tr50.0_Sordered.txt": "com23"
}

inters="/home/matthaus/Documents/data/basedo/BED_files/intersect/reshape/"
gene="intersect_gene_"
#"allquads": [f"{inters}{gene}",f"{inters}{gene}",f"{inters}{gene}",f"{inters}{gene}"],
files={
"allquads": ["allquads_G4AAAA.bed","allquads_G4AABB.bed","allquads_G4ABAA.bed","allquads_G4ABAB.bed","allquads_G4ABBA.bed","allquads_G4ABBB.bed","allquads_G4BAAA.bed","allquads_G4BABA.bed","allquads_G4BABB.bed","allquads_G4BBAA.bed"],
"G4": ["endoquad_G4.bed","imgrinder_GQ.bed","non-bdb_GQ.bed"],
"IM": ["imgrinder_IM.bed","imseeker_imotif.bed"],
"palindrome": ["palindrome_cruci.bed","non-bdb_cruci.bed"],
"TFO": ["gquadR_TFO.bed"],
"HDNA": ["gquadR_HDNA.bed","non-bdb_triplex.bed"],
"Z": ["gquadR_ZDNA.bed","non-bdb_Z.bed"],
"slipped": ["gquadR_slipped.bed","non-bdb_slipped.bed"],
"Satellite": ["dfam_Satellite.bed","repbase_Satellite.bed"],
"APR": ["gquadR_APR.bed","non-bdb_APR.bed"],
"STR": ["gquadR_STR.bed","non-bdb_STR.bed"],
"DR": ["non-bdb_DR.bed"],
"IR": ["palindrome.bed","non-bdb_IR.bed"],
"MR": ["non-bdb_MR.bed"]
}


stats_df=pd.DataFrame()
for struct, sources in files.items():
    for source in sources:
        directory = "/home/matthaus/Documents/data/basedo/BED_files/intersect/reshape/"
        file_name = f"{directory}{gene}{source}"
        df=comm_filter(file_name,comu)
        # Your calculations
        min_val = df['count'].min()
        max_val = df['count'].max()
        median_val = df['count'].median()
        first_quartile_val = df['count'].quantile(0.25)
        third_quartile_val = df['count'].quantile(0.75)
        total=df['count'].sum()
        # Step 1: Total number of rows
        total_rows = len(df)

        # Step 2: Number of rows where 'count' is 0
        rows_with_count_zero = (df['count'] == 0).sum()

        # Step 3: Calculate the percentage
        zero = (rows_with_count_zero / total_rows) * 100

        # Create a DataFrame with these values
        new_stats = {
            'Min': [min_val],
            'First Quartile': [first_quartile_val],
            'Median': [median_val],
            'Third Quartile': [third_quartile_val],
            'Max': [max_val],
            '0%':[zero],
            'total':[total]
        }
        new_row_df = pd.DataFrame(new_stats, index=[source])
        # Save the DataFrame to a CSV file
        stats_df = pd.concat([stats_df, new_row_df])
        stats_df.to_csv('/home/matthaus/Documents/chia-pet_network/results/nonb/V4/stats_summary_gene.csv', index=True,sep="\t")



for struct, sources in files.items():
    # Define the PDF path outside the community loop, so one PDF is created per structure
    pdf_path = f'/home/matthaus/Documents/chia-pet_network/results/nonb/V4/gene/{struct}_densityplot.pdf'
    with PdfPages(pdf_path) as pdf:
        for community_path, community_name in communities.items():
            # Initialize a list to store dataframes for each source within the current community
            sources_dataframes = []
            
            for source in sources:
                directory = "/home/matthaus/Documents/data/basedo/BED_files/intersect/reshape/"
                file_name = f"{directory}{gene}{source}"
                
                # Assuming plot_all function generates and directly saves the plot to the PDF
                # If plot_all is not designed to handle PdfPages, you might need to adjust its implementation
                plot_all(file_name, community_path, pdf)

files={
"allquads": ["allquads_G4AAAA.bed","allquads_G4AABB.bed","allquads_G4ABAA.bed","allquads_G4ABAB.bed","allquads_G4ABBA.bed","allquads_G4ABBB.bed","allquads_G4BAAA.bed","allquads_G4BABA.bed","allquads_G4BABB.bed","allquads_G4BBAA.bed"],
"G4": ["endoquad_G4.bed","imgrinder_GQ.bed","non-bdb_GQ.bed"],
"IM": ["imgrinder_IM.bed","imseeker_imotif.bed"],
"palindrome": ["palindrome_cruci.bed","non-bdb_cruci.bed"],
"TFO": ["gquadR_TFO.bed"],
"HDNA": ["gquadR_HDNA.bed","non-bdb_triplex.bed"],
"Z": ["gquadR_ZDNA.bed","non-bdb_Z.bed"],
"slipped": ["gquadR_slipped.bed","non-bdb_slipped.bed"],
"Satellite": ["dfam_Satellite.bed"],
"APR": ["non-bdb_APR.bed"],
"STR": ["gquadR_STR.bed","non-bdb_STR.bed"],
"DR": ["non-bdb_DR.bed"],
"IR": ["palindrome.bed","non-bdb_IR.bed"],
"MR": ["non-bdb_MR.bed"]
}
gene="intersect_prom_"


stats_df=pd.DataFrame()
for struct, sources in files.items():
    for source in sources:
        directory = "/home/matthaus/Documents/data/basedo/BED_files/intersect/reshape/"
        file_name = f"{directory}{gene}{source}"
        df=comm_filter(file_name,comu)
        # Your calculations
        min_val = df['count'].min()
        max_val = df['count'].max()
        median_val = df['count'].median()
        first_quartile_val = df['count'].quantile(0.25)
        third_quartile_val = df['count'].quantile(0.75)
        total=df['count'].sum()

        # Step 1: Total number of rows
        total_rows = len(df)

        # Step 2: Number of rows where 'count' is 0
        rows_with_count_zero = (df['count'] == 0).sum()

        # Step 3: Calculate the percentage
        zero = (rows_with_count_zero / total_rows) * 100

        # Create a DataFrame with these values
        new_stats = {
            'Min': [min_val],
            'First Quartile': [first_quartile_val],
            'Median': [median_val],
            'Third Quartile': [third_quartile_val],
            'Max': [max_val],
            '0%':[zero],
            'total':[total]
        }
        new_row_df = pd.DataFrame(new_stats, index=[source])
        # Save the DataFrame to a CSV file
        stats_df = pd.concat([stats_df, new_row_df])
        stats_df.to_csv('/home/matthaus/Documents/chia-pet_network/results/nonb/V4/stats_summary_prom.csv', index=True,sep="\t")




for struct, sources in files.items():
    # Define the PDF path outside the community loop, so one PDF is created per structure
    pdf_path = f'/home/matthaus/Documents/chia-pet_network/results/nonb/V4/prom/{struct}_densityplot.pdf'
    with PdfPages(pdf_path) as pdf:
        for community_path, community_name in communities.items():
            # Initialize a list to store dataframes for each source within the current community
            sources_dataframes = []
            
            for source in sources:
                directory = "/home/matthaus/Documents/data/basedo/BED_files/intersect/reshape/"
                file_name = f"{directory}{gene}{source}"
                
                # Assuming plot_all function generates and directly saves the plot to the PDF
                # If plot_all is not designed to handle PdfPages, you might need to adjust its implementation
                plot_all(file_name, community_path, pdf)



def plot_all_exon(str_df, comu_dict, pdf):
    result_df = comm_filter_exon(str_df, comu_dict)
    file_basename = os.path.basename(str_df)
    structure_name = os.path.splitext(file_basename)[0]
    sns.set(style="whitegrid")
    communities = result_df['community'].unique()
    n_colors = len(communities)
    palette = sns.color_palette("coolwarm", n_colors=n_colors)
    
    fig, axs = plt.subplots(2, 3, figsize=(20, 12))

    sns.kdeplot(data=result_df, x="count", hue="community", fill=True, common_norm=False, palette=palette, ax=axs[0, 0])
    axs[0, 0].set_title(f"Density Curves of Counts for Each Community {community_name}- {source}")

    sns.kdeplot(data=result_df, x="count_norm", hue="community", fill=True, common_norm=False, palette=palette, ax=axs[0, 1])
    axs[0, 1].set_title(f"Density Curves of Counts Normalised {community_name}- {source}")

    sns.kdeplot(data=result_df, x="length_norm", hue="community", fill=True, common_norm=False, palette=palette, ax=axs[0, 2])
    axs[0, 2].set_title(f"Density Curves of Length Normalised {community_name}- {source}")

    sns.violinplot(data=result_df, x="community", y="count", inner="quartile", palette="Set3", ax=axs[1, 0])
    axs[1, 0].set_title(f"Violin Plot of Count for Each Community {community_name}- {source}")
    axs[1, 0].tick_params(axis='x', rotation=45)

    sns.violinplot(data=result_df, x="community", y="count_norm", inner="quartile", palette="Set3", ax=axs[1, 1])
    axs[1, 1].set_title(f"Violin Plot of Counts Normalised  {community_name}- {source}")
    axs[1, 1].tick_params(axis='x', rotation=45)

    sns.violinplot(data=result_df, x="community", y="length_norm", inner="quartile", palette="Set3", ax=axs[1, 2])
    axs[1, 2].set_title(f"Violin Plot of Length Normalised {community_name}- {source}")
    axs[1, 2].tick_params(axis='x', rotation=45)    
    plt.tight_layout()
    pdf.savefig(fig)  # Save the current figure into the PDF file
    #plt.show()
    plt.close()


    



def comm_filter_exon(str_df, comu_dict):
    # Assume exon_com, df_maker_density, and read_community_genes are defined elsewhere and work as expected
    exondf = exon_com(comu)  # Not clear how 'comu' is defined, assuming it's provided elsewhere
    df_struct = df_maker_density(str_df)
    dgene = read_community_genes(comu_dict)
    #print("exondf:",exondf)

    # Create a new column for the actual gene ID by removing the exon part
    df_struct['actual_gene_id'] = df_struct['gene_id'].apply(lambda x: x.split('_')[0])
    # Step 2: Perform the merge
    merged_df = pd.merge(df_struct, exondf,  left_on='gene_id', right_on='exon_id', how='outer')
    # Step 3: Adjust missing values for 'count', 'count_norm', and 'length_norm'

    # Fill missing 'count' with 0
    merged_df['count'] = merged_df['count'].fillna(int(0))
    merged_df['count_norm'] = merged_df['count_norm'].fillna(int(0))
    merged_df['length_norm'] = merged_df['length_norm'].fillna(int(0))
    #print("zgeiohghiogrjio",merged_df)
    # Drop the redundant 'gene_id' column if needed (since 'exon_id' and 'gene_id' are essentially the same)
    merged_df.drop('gene_id_x', axis=1, inplace=True)
    #merged_df.drop('actual_gene_id', axis=1, inplace=True)
    merged_df
    # Optionally, rename 'exon_id' to 'actual_gene_id' if that's a requirement, but it seems from your description
    # that 'actual_gene_id' is already correctly derived in 'df_struct'
    # merged_df.rename(columns={'exon_id': 'actual_gene_id'}, inplace=True)
    return merged_df

def exon_presence_plot(str_df, comu_dict):
    df_struct = df_maker_density(str_df)  # Assuming this function prepares your DataFrame from a structured string
    dgene = read_community_genes(comu_dict)  # Assuming this function reads gene-community mappings
    df_struct['actual_gene_id'] = df_struct['gene_id'].apply(lambda x: x.split('_')[0])
    
    dfs = []
    for k, v in dgene.items():
        community = k
        kc = df_struct[df_struct['actual_gene_id'].isin(v)].copy()  # Make a copy to avoid modifying the original DataFrame
        kc['community'] = community  # Assigning the community value to the 'community' column
        #print(kc)

        dfs.append(kc)  # Append the modified DataFrame to the list
    result_df = pd.concat(dfs, ignore_index=True)
    df_filtered = result_df[['gene_id', 'community']]
    df_filtered = df_filtered.rename(columns={'community': source})

    return df_filtered


# Specified order for the first community categories
order_first_community = [
    "Lamina", "Near-Lm2", "Near-Lm1", "Interior-Act3", "Lamina-Like",
    "Interior-Repr1", "Interior-Act2", "Interior-Repr2", "Interior-Act1", "Speckle"
]

# Specified order for the second community categories
order_second_community = [
    "G1", "G2", "G3", "G4", "G5", "G6", "G7", "G8", "G9", "G10",
    "G11", "G12", "G13", "G14", "G15", "G16", "G17", "G18", "G19", "G20",
    "G21", "G22", "G23"
]
z=0


files={
"G4": ["endoquad_G4.bed","imgrinder_GQ.bed","non-bdb_GQ.bed"],
"IM": ["imgrinder_IM.bed","imseeker_imotif.bed"],
"palindrome": ["palindrome_cruci.bed","non-bdb_cruci.bed"],
"TFO": ["gquadR_TFO.bed"],
"HDNA": ["gquadR_HDNA.bed","non-bdb_triplex.bed"],
"Z": ["gquadR_ZDNA.bed","non-bdb_Z.bed"],
"slipped": ["gquadR_slipped.bed","non-bdb_slipped.bed"],
"Satellite": ["dfam_Satellite.bed"],
"APR": ["non_bdb_APR.bed"],
"STR": ["gquadR_STR.bed","non_bdb_STR.bed"],
"DR": ["non_bdb_DR.bed"],
"IR": ["palindrome.bed","non-bdb_IR.bed"],
"MR": ["non_bdb_MR.bed"]
}

gene="intersect_exon_"
for community_path, community_name in communities.items():
    ldf = []    
    for struct, sources in files.items():
        for source in sources:
                directory = "/home/matthaus/Documents/data/basedo/BED_files/intersect/reshape/"
                file_name = f"{directory}{gene}{source}"

                ck=exon_presence_plot(file_name,community_path)
                ldf.append(ck)  # Append the modified DataFrame to the list
        
    result_df = pd.concat(ldf, ignore_index=True)
    df=result_df
    print(df)

    # Generate summary DataFrame
    # Initialize an empty DataFrame for the summary
    summary_df = pd.DataFrame()

    # Iterate through each column except 'gene_id'
    for col in df.columns[1:]:
        # Count the occurrence of each unique value in the current column
        counts = df[col].value_counts().to_frame(name=col)
        # If the summary DataFrame is empty, initialize it with the counts
        if summary_df.empty:
            summary_df = counts
        else:
            # If summary_df already exists, join the new counts
            summary_df = summary_df.join(counts, how='outer')

    # Replace NaN with 0 to indicate no occurrences
    summary_df = summary_df.fillna(0)

    # Calculate proportions for each group value within each bed file category
    proportions_df = summary_df.div(summary_df.sum(axis=1), axis=0)

    is_first_community = proportions_df.index[0] in order_first_community

    # Reorder 'proportions_df' according to the specified order for the community
    if is_first_community:
        ordered_index = order_first_community
    else:
        ordered_index = order_second_community

    # Reindex 'proportions_df' to match the desired order
    # Note: Ensure 'proportions_df' index matches the categories mentioned in the order lists
    proportions_df = proportions_df.reindex(ordered_index)

    # Calculate the sum of proportions for each group across all categories
    sum_proportions = proportions_df.sum(axis=0)

    # Sort the columns (groups) in 'proportions_df' based on the sum of proportions (ascending order so highest is at bottom)
    sorted_columns = sum_proportions.sort_values(ascending=False).index

    # Reorder 'proportions_df' columns based on sorted_columns
    proportions_df_sorted = proportions_df[sorted_columns]

    # Initialize a Plotly figure
    fig = go.Figure()

    # Add a bar for each group value (column) in the sorted 'proportions_df'
    for group_value in proportions_df_sorted.columns:
        fig.add_trace(go.Bar(
            x=proportions_df_sorted.index,  # Bed file categories as x-axis
            y=proportions_df_sorted[group_value],  # Proportions of occurrences for this group value
            name=group_value,  # Legend name
        ))

    # Update layout for a stacked bar plot
    fig.update_layout(
        barmode='stack',
        title='Diversity of Structures Across Groups (Proportions, Ordered)',
        xaxis=dict(
            title='Bed File Category',
            tickangle=-45,  # Optionally rotate labels for better readability
        ),
        yaxis=dict(
            title='Proportion of Occurrences',
            tickformat=".0%",
        ),
        legend_title='Group Value',
    )
    if z==0:
        z+=1
        # Show the figure
        fig.show()
        fig.write_html("/home/matthaus/Documents/chia-pet_network/results/nonb/exon/Spin_EXON.html")
    else:
        fig.show()
        fig.write_html("/home/matthaus/Documents/chia-pet_network/results/nonb/exon/Com23_EXON.html")

            



for struct, sources in files.items():
    # Define the PDF path outside the community loop, so one PDF is created per structure
    pdf_path = f'/home/matthaus/Documents/chia-pet_network/results/nonb/V4/exon/{struct}_densityplot.pdf'
    with PdfPages(pdf_path) as pdf:
        for community_path, community_name in communities.items():
            # Initialize a list to store dataframes for each source within the current community
            sources_dataframes = []
            
            for source in sources:
                directory = "/home/matthaus/Documents/data/basedo/BED_files/intersect/reshape/"
                file_name = f"{directory}{gene}{source}"
                # Assuming plot_all function generates and directly saves the plot to the PDF
                # If plot_all is not designed to handle PdfPages, you might need to adjust its implementation
                #plot_all_exon(file_name, community_path, pdf)

files={
"G4": ["endoquad_G4.bed","imgrinder_GQ.bed","non-bdb_GQ.bed"],
"IM": ["imgrinder_IM.bed","imseeker_imotif.bed"],
"palindrome": ["palindrome_cruci.bed","non-bdb_cruci.bed"],
"TFO": ["gquadR_TFO.bed"],
"HDNA": ["gquadR_HDNA.bed","non-bdb_triplex.bed"],
"Z": ["gquadR_ZDNA.bed","non-bdb_Z.bed"],
"slipped": ["gquadR_slipped.bed","non-bdb_slipped.bed"],
"Satellite": ["dfam_Satellite.bed"],
"APR": ["non_bdb_APR.bed"],
"STR": ["gquadR_STR.bed","non_bdb_STR.bed"],
"DR": ["non_bdb_DR.bed"],
"IR": ["palindrome.bed","non-bdb_IR.bed"],
"MR": ["non_bdb_MR.bed"]
}
stats_df=pd.DataFrame()
for struct, sources in files.items():
    for source in sources:
        directory = "/home/matthaus/Documents/data/basedo/BED_files/intersect/reshape/"
        file_name = f"{directory}{gene}{source}"
        df=comm_filter_exon(file_name,comu)
        # Your calculations
        min_val = df['count'].min()
        max_val = df['count'].max()
        median_val = df['count'].median()
        first_quartile_val = df['count'].quantile(0.25)
        third_quartile_val = df['count'].quantile(0.75)
        # Step 1: Total number of rows
        total_rows = len(df)
        total=df['count'].sum()

        # Step 2: Number of rows where 'count' is 0
        rows_with_count_zero = (df['count'] == 0).sum()

        # Step 3: Calculate the percentage
        zero = (rows_with_count_zero / total_rows) * 100

        # Create a DataFrame with these values
        new_stats = {
            'Min': [min_val],
            'First Quartile': [first_quartile_val],
            'Median': [median_val],
            'Third Quartile': [third_quartile_val],
            'Max': [max_val],
            '0%':[zero],
            'total':[total]
        }
        new_row_df = pd.DataFrame(new_stats, index=[source])
        # Save the DataFrame to a CSV file
        stats_df = pd.concat([stats_df, new_row_df])
        stats_df.to_csv('/home/matthaus/Documents/chia-pet_network/results/nonb/V4/stats_summary_exon.csv', index=True,sep="\t")
