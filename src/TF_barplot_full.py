from collections import defaultdict
import os
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import random
from matplotlib.patches import Patch
import os
#os.environ['MPLBACKEND'] = 'Agg'
import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns
import plotly.graph_objects as go


# Specify the directory path
intersect = '/home/matthaus/Documents/data/basedo/tfpeak/1000intersect/'
files = [file for file in os.listdir(intersect) if os.path.isfile(os.path.join(intersect, file))]
print(files)

def lib_create(aaa):
    ########Lib_count
    #df_tf=pd.read_csv("/home/matthaus/Documents/data/basedo/tfpeak/remap2022_all_macs2_hg38_v1_0_filtered_kept_info_double_tiret_tf_interest.bed",sep='\t', header=None)
    df_tf=pd.read_csv("/home/matthaus/Documents/data/basedo/tfpeak/remap2022_all_macs2_hg38_v1_0_filtered_kept_info_double_tiret_tf_interest.bed",sep='\t', header=None)
    df_tf.columns = ['chr', 'start', 'end', 'tf_name', 'score', 'strand']
    ## parse the df and find the right name
    df_tf_required = df_tf[['tf_name']].copy()
    df_tf_required['tf_name'] = df_tf_required['tf_name'].apply(lambda x: x.split('__')[1] if '__' in x else x)
    #Count the number of time each TF apears in the file
    df_tf_required = df_tf_required.groupby('tf_name').size().rename("peak_count").reset_index()
    #df_tf_required.to_csv('/home/matthaus/Documents/data/basedo/tfpeak/clean/TF_filter51000_nb.csv',sep='\t', index=False)

df_tf_required=pd.read_csv('/home/matthaus/Documents/data/basedo/tfpeak/clean/TF_filter51000_nb.csv',sep='\t')
print(df_tf_required)
#sum_tf=df_tf_required['peak_count'].sum()

#print(sum_tf)


def df_maker(file_path):
    '''df_requierd is the base count df without normalisation 
    tf_name   IR
    0        AATF   73
    1        AFF1  206
    2        AFF4   88
    3        AGO1  677
    4         AHR  573
    ...       ...  ...
    1035  ZSCAN5A   24
    1036  ZSCAN5C   50
    1037     ZXDB  126
    1038     ZXDC   84
    1039     ZZZ3   40

    '''
    # Read the file into a DataFrame
    df = pd.read_csv(file_path, sep='\t', header=None)
    suffix = file_path.split('intersect_')[-1].split('.bed')[0]
    if df.shape[1] == 10:
        df.columns = ['chr_start', 'start', 'end', 'tf_name', 'score', 'strand',
                      'chr_end', 'structure_start', 'structure_end', 'struct']
    if df.shape[1] == 11:
        df.columns = ['chr_start', 'start', 'end', 'tf_name', 'score', 'strand',
                      'chr_end', 'structure_start', 'structure_end', 'struct', 'sequ']
    elif df.shape[1] == 12:
        df.columns = ['chr_start', 'start', 'end', 'tf_name', 'score', 'strand',
                      'chr_end', 'structure_start', 'structure_end', 'struct','strandstr','sequ']
    unique_df = df.drop_duplicates(subset=['chr_start', 'start', 'end']) #harvest only one structure per peak
    df_required = unique_df[['tf_name', 'struct']].copy()
    df_required['tf_name'] = df_required['tf_name'].apply(lambda x: x.split('__')[1] if '__' in x else x)
    df_required = df_required.groupby('tf_name').size().rename(f"{suffix}").reset_index()

    print(df_required)
    return df_required


def TF_barplot_all(file_paths, df_tf_required):
    """
    Processes BED files, merges with a reference DataFrame, calculates ratios, filters based on criteria, 
    and generates bar plots saved as HTML files.
    
    Parameters:
    - file_paths: List of paths to BED files.
    - df_tf_required: DataFrame containing required transcription factors (TF) with 'tf_name' and 'peak_count' columns.
    """
    

    for file in file_paths:
        # Assuming df_maker is a function that reads a file and returns a DataFrame
        df = df_maker(file)
        name = file.split('intersect_')[-1].split('.bed')[0]
        df.set_index('tf_name', inplace=True)
        df = df.astype(int)

        df = pd.merge(df, df_tf_required, left_on='tf_name', right_on='tf_name', how='left')

        # Calculate the ratio of structures to peaks
        df['structure_to_peak_ratio'] = df[name] / df['peak_count']
        df.set_index('tf_name', inplace=True)

        #filtered_df = df[df['structure_to_peak_ratio'] >= 1/20]
        #filtered_df = df[df['structure_to_peak_ratio']]
        #filtered_df['structure_to_peak_ratio'] = filtered_df['structure_to_peak_ratio'] * 100
        df = df.sort_values(by='structure_to_peak_ratio', ascending=False)
        df = df.sort_values(by='structure_to_peak_ratio', ascending=False)
        print(df)
        df.to_csv(f"/home/matthaus/Documents/chia-pet_network/results/nonb/TF/clean/csv/TF_{name}.csv",sep="\t")

        sum_tf=df['peak_count'].sum()

        sum_STR=df[f'{name}'].sum()
        tresh=(sum_STR/sum_tf)
        print(sum_STR,sum_tf,tresh)
        tresh2=tresh*2
        # Create and configure the bar plot
        fig = go.Figure(go.Bar(x=df.index, y=df['structure_to_peak_ratio']))

        fig.add_hline(y=tresh, line_color="red")
        fig.add_hline(y=tresh2, line_color="blue")
        fig.update_layout(
            title='TF counts',
            xaxis_title='TF Name',
            yaxis_title=f'Count of {name}',
            xaxis={'categoryorder':'total descending'}
        )

        # Output path adjustment based on your existing pattern
        output_path = f"/home/matthaus/Documents/chia-pet_network/results/nonb/TF/clean/barplot/double/Barplot_{name}_double.html"
        fig.write_html(output_path)
        print(f"Plot saved to {output_path}")

# String to add at the beginning
path = '/home/matthaus/Documents/data/basedo/tfpeak/1000intersect/'

# Add the prefix to each value in the list
modified_list = [path + value for value in files]

TF_barplot_all(modified_list,df_tf_required)

