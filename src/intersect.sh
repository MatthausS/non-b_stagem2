#!/bin/bash
# Check if two arguments are provided
if [ "$#" -ne 3 ]; then
    echo "Usage: $0 annotation.bed structure.bed outpath
     need to be in a bedtools environement"
    exit 1
fi
# structures=("DZDNA_HG38.bed" "gquadR_ZDNA.bed" "gquadR_slipped.bed" "gquadR_TFO.bed" "IMSEEKER.bed" "PiMS2a.bed" "triplex.bed" "palindromes_cruci.bed" "Repeatmasker_Satellite.bed" "dfam_Satellite.bed" "gquadR_APR.bed" "palindromes.bed" "gquadR_STR.bed")
#
# Assign input file names
annotation_bed="$1"
structure_bed="$2"
out="$3"
# Extract name of the structure file
structure_name=$(basename "$structure_bed" | cut -d. -f1)

# Output directory
output_dir=$(dirname "$structure_bed")

# Perform bedtools intersect
output_file=${out}"intersect_"${structure_name}".bed"
bedtools intersect -wa -wb -F 1 -a "$annotation_bed" -b "$structure_bed" > "$output_file"

echo "Intersect output saved to: $output_file"
